ARG REPO=mcr.microsoft.com/dotnet/core/aspnet
FROM $REPO:3.1-alpine3.12

ENV \
    # Disable the invariant mode (set in base image)
    DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false \
    # Enable correct mode for dotnet watch (only mode supported in a container)
    DOTNET_USE_POLLING_FILE_WATCHER=true \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    # Skip extraction of XML docs - generally not useful within an image/container - helps performance
    NUGET_XMLDOC_MODE=skip \
    ASPNETCORE_ENVIRONMENT=Development \
    ASPNETCORE_URLS=http://+:80  
	
RUN apk add --no-cache icu-libs

RUN apk add \
        --no-cache \
        --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
        libgdiplus

EXPOSE 80

WORKDIR /app
COPY . .
ENTRYPOINT ["dotnet", "POCSignalR.dll"]
